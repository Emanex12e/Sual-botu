from telethon import TelegramClient, events, errors
import random
from telethon import TelegramClient, events, Button
from telethon.errors.rpcerrorlist import ChatAdminRequiredError
from telethon.tl.functions.messages import SendMessageRequest

api_id = 7562178
api_hash = 'df85e3f5a6783284aee424f57fb17ace'
bot_token = ''

client = TelegramClient('bot_session', api_id, api_hash).start(bot_token=bot_token)

# Kimya soruları ve cevapları
ksual = [{'sual': 'Elementlerin simvolları nədir?', 'cavab': 'C'}, 
         {'sual': 'En çox kullanılan element nədir?', 'cavab': 'Oksigen'},
         {'sual': 'Atomun bəzən parçalanması olayı nədir?', 'cavab': 'Fisiya'}]

# Fizika soruları ve cevapları
fsual = [{'sual': 'Hərəkətli cisimlərin yerini dəyişməsinə nə ad verilir?', 'cavab': 'İrəli mənbə'},
         {'sual': 'Hərəkətli cisimlərin saxlanılma qanunu kimə aid edilir?', 'cavab': 'Newton'},
         {'sual': 'Səs dalğaları hansı mədium üzərində hərəkət edir?', 'cavab': 'Kütləvi'}]

# Riyaziyyat soruları ve cevapları
rsual = [{'sual': '3+5-nin cavabı nədir?', 'cavab': '8'},
         {'sual': '12-nin 2/3-dən çıxması nədir?', 'cavab': '6'},
         {'sual': '5*5/5+5-nin cavabı nədir?', 'cavab': '11'}]

# Informatika soruları ve cevapları
isual = [{'sual': 'İlk kompüter hansı ildə hazırlanmışdır?', 'cavab': '1936'},
         {'sual': 'Python proqramlaşdırma dilinin yaradıcısı kimdir?', 'cavab': 'Guido van Rossum'},
         {'sual': 'Git proqramının əsas funksiyası nədir?', 'cavab': 'Versiyaların idarə olunması'}]


#help komuttu

@client.on(events.NewMessage(pattern='/help'))
async def help_handler(event):
    await event.respond("""
    Yeniden salam 🤓\n\nAşağıda verdiyim əmrləri İstifadə etsən, həmin fəndən sənə Sual göndərəcəm. əgər suala doğru cavab versən, "terbikler" mesajı alacaqsan.\n\n❓Unutma: Mən, bir botam. Cavabı verərkən edəcəyin kiçik bir hərf səhvi belə, məni yanlış yönləndirə bilər.\n\nƏmirlər👇\n\n/ksual - Kimya sualı\n/fsual - Fizika sualı\n/rsual - Riyaziyyat sualı\n/isual - İnformatika sualı\n\n📖 Botda öz Sualını görmək istəyirsən? onda Sahibimə yaz və suallarını ona göndər ✔️""")


#start mesaji
@client.on(events.NewMessage(pattern='/start'))
async def start(event):

    # 2 düğme ve bir uzun düğme oluşturun
    button1 = Button.url('SAHİBİM 👨🏻‍💻', 'http://t.me/Elvin_001')
    button2 = Button.url('KANAL 📣', 'https://t.me/ElvinWorld')
    button3 = Button.url('🎈 Meni qrupa elave et 🎈', 'https://t.me/EnSualbot?startgroup=a')

    # Düğmeleri mesaja ekleyin
    await event.respond('Salam👋\n\nMən sənin Sual-Cavab etməyin üçün hazırlanmışam. \n\n/help əmr ile mənim haqqımda daha çox məlumat ala bilersen.\n\n      "burda reklamınız ola bilərdi"\n',buttons=[[button1, button2], [button3]])

# Komutların işlevleri
@client.on(events.NewMessage(pattern='/ksual'))
async def ksual_command(event):
    # random kimya sorusu
    sual = random.choice(ksual)['sual']
    await event.respond(sual)

@client.on(events.NewMessage(pattern='/fsual'))
async def fsual_command(event):
    # random fizika sorusu
    sual = random.choice(fsual)['sual']
    await event.respond(sual)

@client.on(events.NewMessage(pattern='/rsual'))
async def rsual_command(event):
    # random riyaziyyat sorusu
    sual = random.choice(rsual)['sual']
    await event.respond(sual)

@client.on(events.NewMessage(pattern='/isual'))
async def isual_command(event):
    # random informatika sorusu
    sual = random.choice(isual)['sual']
    await event.respond(sual)


# Kullanıcının cevabını kontrol eden işlev
@client.on(events.NewMessage)
async def answer(event):
    # Botun mesajlarına yanıt vermeyin
    if event.is_private or event.is_group:
        # Kullanıcının mesajını alın
        user_input = event.message.message.strip().lower()
        # Kullanıcının yanıtı doğru mu?
        if user_input in [answer['cavab'].lower() for answer in ksual+fsual+rsual+isual]:
            await event.respond('Təbriklər, doğru cavab!')
        else:
            # Doğru cevabı bulun
            for answer in ksual+fsual+rsual+isual:
                if user_input == answer['cavab'].lower():
                    correct_answer = answer['cavab']
                    await event.respond(f"Yanlış cavab. Doğru cavab {correct_answer} idi.")
                    break
  
        
  
client.run_until_disconnected()
      
db["key"] = "value"
db["key"] = "value"
from replit import db
db["APİ_KEY"] = "7562178"
db["APİ_İD"] = "7562178"
db["7562178"]
